import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import './index.css';
import App from './app/layouts/App';

const rootEl = document.getElementById('root');
let render = () => {
    ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>
    , rootEl);
}

render();

