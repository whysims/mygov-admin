import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import EventDashboard from "../../features/event/EventDashboard/EventDashboard";
import ClientPage from '../../features/client/ClientPage/ClientPage';
import Navbar from "../../features/Navbar/Navbar/Navbar";
import ClientSingle from '../../features/client/ClientSingle/ClientSingle';
import HomePage from '../../features/home/HomePage';
import StatusPage from '../../features/status/StatusPage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <div className="mygov__content">
          <Route exact path='/' component={HomePage}/>
          <Route path='/clients' component={ClientPage}/>
          <Route path='/client/:id' component={ClientSingle}/>
          <Route path='/event/:id' component={ClientPage}/>
          <Route path='/status' component={StatusPage}/>
        </div>
      </div>
    );
  }
}

export default App;
