import React, { Component } from 'react'
import { Grid } from 'semantic-ui-react';
import EventList from "../EventList/EventList";
import BudgetList from "../BudgetList/BudgetList";

class EventDashboard extends Component {
  render() {
    return (
      <Grid>
        <Grid.Column width={10}>
            <h2>Last Actions</h2>
            <EventList/>
        </Grid.Column>
        <Grid.Column width={6}>
            <h2>Last Budgets</h2>
            <BudgetList />
        </Grid.Column>
      </Grid>
    )
  }
}

export default EventDashboard;