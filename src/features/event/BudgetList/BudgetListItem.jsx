import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';

class BudgetListItem extends Component {
    render() {
        return (
            <Segment.Group>
                <Segment padded color='blue' className="budget__segment">
                    <b>Gabinete Tal</b> assinou o plano <b>Premium</b>
                </Segment>
            </Segment.Group>
        );
    }
}

export default BudgetListItem;