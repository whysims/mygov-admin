import React, { Component } from 'react';
import BudgetListItem from "./BudgetListItem";

class BudgetList extends Component {
    render() {
        return (
            <div>
                <BudgetListItem/>
                <BudgetListItem/>
                <BudgetListItem/>
                <BudgetListItem/>
                <BudgetListItem/>
            </div>
        );
    }
}

export default BudgetList;