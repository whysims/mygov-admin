import React, { Component } from 'react';
import { Segment, Item } from 'semantic-ui-react';

class EventListItem extends Component {
    render() {
        return (
                <Segment.Group>
                    <Segment>
                        <Item.Group>
                            <Item>
                                <Item.Content>
                                Action from Admins
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    </Segment>
                    <Segment>
                        <h1>User Creation</h1>
                    </Segment>
                </Segment.Group>
        )
    }
}

export default EventListItem;