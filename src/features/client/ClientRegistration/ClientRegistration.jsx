import React, { Component } from 'react';

class ClientRegistration extends Component {
    render() {
        return (
            <div>
                <Segment>
                <Form>
                    <Form.Field>
                        <label>First Name</label>
                        <input placeholder="John"/>
                    </Form.Field>
                    <Form.Field>
                        <label>Last Name</label>
                        <input placeholder="Doe"/>
                    </Form.Field>
                    <Form.Field>
                        <label>Office</label>
                        <input placeholder="John Doe Office"/>
                    </Form.Field>
                    <Form.Field>
                        <label>Plans</label>
                        <select>
                            <option value="1">Basic Prom.</option>
                            <option value="2">Basic</option>
                            <option value="3">Premium</option>
                        </select>
                    </Form.Field>
                    <Button type='submit'>Submit</Button>
                </Form>
            </Segment>
            </div>
        );
    }
}

export default ClientRegistration;