import React, { Component } from 'react'
import { Menu, List, Header, Icon} from 'semantic-ui-react';
import { NavLink, Link } from  'react-router-dom';


class Navbar extends Component {
  render() {
    return (
            <Menu className="mygov__navbar" fixed="top">
            
            <List selection className="mygov__navbar-nav" verticalAlign='middle'>
            <Header>
              Usuário Logado

            </Header>
            <List.Item>
              <List.Content>
                <List.Header className="mygov__navbar-item"><Menu.Item as={NavLink} to='/clients'><Icon name="user circle" /> Clientes</Menu.Item></List.Header>
              </List.Content>
            </List.Item>
            <List.Item>
              <List.Content>
                <List.Header className="mygov__navbar-item"><Menu.Item as={NavLink} to='/status'><Icon name="line graph" /> Status</Menu.Item></List.Header>
              </List.Content>
            </List.Item>
          </List>
            </Menu>
    )
  }
}

export default Navbar;